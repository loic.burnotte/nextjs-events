import Link from 'next/link'
import Head from 'next/head'
import { Event } from '@/models/event.model'
import { getFilteredData } from '@/helpers/crud'
import EventList from '@/components/events/event-list'
import ResultsTitle from '@/components/events/results-title'

function DetailsPage({
  events,
  date: { year, month },
  hasError,
}: {
  events: Event[] | undefined
  date: {
    year: number
    month: number
  }
  hasError?: boolean
}) {
  // ALTERNATIVE WAY
  // const router = useRouter()
  // const filteredData = router.query.slug

  /* [SEO] */
  let header = (
    <Head>
      <title>Filtered Events</title>
      <meta name="description" content={`A list of filtered events.`} />
    </Head>
  )

  if (hasError)
    return (
      <>
        {header}
        <p>Invalid filter. Please adjust the filters</p>
      </>
    )

  if (!events || events.length === 0)
    return (
      <div className="text-center">
        {header}
        <div className="error my-6">
          <p>No events found for the chosen filter!</p>
        </div>
        <Link href="/events" className="btn">
          Show All Events
        </Link>
      </div>
    )

  header = (
    <Head>
      <title>Filtered Events</title>
      <meta name="description" content={`All events for ${month}/${year}`} />
    </Head>
  )

  const date = new Date(year, month - 1)
  return (
    <div className="pt-4">
      <ResultsTitle date={date} className="mb-8" />
      <EventList list={events} />
    </div>
  )
}

// SERVER SIDE or CLIENT SIDE data fetching
export async function getServerSideProps(context: { params: { slug: string[] } }) {
  const filteredData = context.params.slug

  const filteredYear = filteredData[0]
  const filteredMonth = filteredData[1]

  const numYear = +filteredYear
  const numMonth = +filteredMonth
  if (
    isNaN(numYear) ||
    isNaN(numMonth) ||
    numYear > new Date().getFullYear() + 10 ||
    (numYear < new Date().getFullYear() - 10 && numMonth > 0 && numMonth < 12)
  )
    return { props: { hasError: true } }

  const filteredEvents = await getFilteredData<Event>({
    filters: {
      year: numYear,
      month: numMonth,
    },
    tableName: 'events',
  })

  return {
    props: {
      events: filteredEvents,
      date: {
        year: numYear,
        month: numMonth,
      },
      hasError: false,
    },
  }
}

export default DetailsPage
