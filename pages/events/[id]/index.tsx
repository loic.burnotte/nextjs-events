import Link from 'next/link'
import Head from 'next/head'
import Image from 'next/image'
import { useContext } from 'react'
import Edit from '@/components/icons/edit'
import { useSession } from 'next-auth/react'
import { useRouter } from 'next/navigation'
import { formatDate } from '@/helpers/common'
import type { Event } from '@/models/event.model'
import DeleteIcon from '@/components/delete-icon'
import Location from '@/components/icons/location'
import Calendar from '@/components/icons/calendar'
import type { ResponseData } from '@/pages/api/events'
import { deleteItem, getAll, getItem } from '@/helpers/crud'
import NotificationContext from '@/store/notification-context'

function DetailsPage({ event }: { event?: Event }) {
  const notificationCtx = useContext(NotificationContext)
  const { status } = useSession()
  const router = useRouter()

  if (!event)
    return (
      <div className="text-center">
        <Head>
          <title></title>
          <meta name="description" content="Find a lot of great events" />
        </Head>
        <div className="error my-6">
          <p>No event found !</p>
        </div>
        <Link href="/events" className="btn">
          Show All Events
        </Link>
      </div>
    )

  async function handleConfirmDelete() {
    if (!event) return

    if (status !== 'authenticated') {
      router.push('/auth')
      return
    }

    const res = await deleteItem(event.id, 'events')
    if (res.success) {
      router.push('/events')
      notificationCtx.showNotification({
        title: 'Success!',
        message: `The event '${event.title}' has been successfully deleted!`,
        status: 'success',
      })
    } else {
      notificationCtx.showNotification({
        title: 'Error',
        message: `When deleting the event ${event.title}`,
        status: 'error',
      })
    }
  }

  return (
    <div className="h-full">
      {/* [SEO] */}
      <Head>
        <title>{event.title}</title>
        <meta name="description" content={event.summary + ' ' + event.description} />
      </Head>

      <div className="text-white absolute top-24 right-5 flex flex-col gap-6 bg-slate-400 rounded-xl border-2 py-4 px-2 border-white">
        <Link href={`/events/${event.id}/edit`} className="text-white" title="Edit">
          <Edit />
        </Link>
        <DeleteIcon message="Are you sure you want to delete this event ?" onConfirm={handleConfirmDelete} />
      </div>

      <div className="bg-gradient-to-bl from-teal-600 to-cyan-800 h-1/3 w-full items-center flex flex-col pt-8">
        <h1 className="text-5xl text-white">{event.title}</h1>
        <div className="center-el justify-center text-white ">
          <Calendar />
          <time className="text-white font-medium ml-2">{formatDate(event.date)}</time>
        </div>
      </div>
      <div className="absolute top-1/4 left-1/2 transform -translate-x-1/2">
        {event.image && (
          <Image
            src={`/${event.image}`}
            className="rounded-full aspect-square object-cover select-none"
            alt={event.title}
            height="280"
            width="280"
            sizes="max-width: 768px"
            quality={50}
          />
        )}
      </div>
      <div className="bg-slate-100 h-2/3 text-center pt-44">
        {event.summary && <h4 className="my-4">{event.summary}</h4>}
        {event.description && <div className="justify text-black">{event.description}</div>}
        <div className="center-el justify-center my-8 max-w-3xl m-auto p-6 rounded-md bg-slate-700 text-white">
          <Location />
          <p className="italic ml-2">{event.address}</p>
        </div>
      </div>
    </div>
  )
}

export default DetailsPage

export async function getStaticPaths() {
  const events = await getAll<Event>('events')
  const paths = events.map(({ id }) => ({ params: { id } }))

  return {
    paths,
    fallback: 'blocking',
  }
}

export async function getStaticProps(context: { params: { id: string } }) {
  const eventId = context.params.id
  const response = await getItem<ResponseData>(eventId, 'events')
  const event = response?.event

  // if (!event) return { notFound: true }

  return {
    props: {
      event: event || null,
    },
    revalidate: 1,
  }
}

// export async function generateStaticParams() {
//   const events = await db.events.findMany();

//   return events.map((event) => {
//     return {
//       id: event.id.toString(),
//     };
//   });
// }
