'use client'

import React, { useContext } from 'react'
import { useRouter } from 'next/navigation'
import type { FormState } from '@/pages/events/create'
import EventForm from '@/components/events/event-form'
import { editItem, getAll, getItem } from '@/helpers/crud'
import type { Event, NewEvent } from '@/models/event.model'
import NotificationContext from '@/store/notification-context'
import { Modal, ModalBody, ModalHeader, ModalContent, useDisclosure } from '@nextui-org/react'

interface EditEventPageProps {
  event: Event | null
}

export default function EditEventPage({ event }: EditEventPageProps) {
  const router = useRouter()
  const notificationCtx = useContext(NotificationContext)
  const { isOpen, onClose } = useDisclosure({ isOpen: true })

  function handleClose() {
    onClose()
    router.push(`/events/${event?.id}`)
  }

  async function submitEvent(updatedEvent: NewEvent) {
    if (!event) return

    notificationCtx.showNotification({
      title: `Creating the new event "${updatedEvent.title}"`,
      message: 'In Progress...',
      status: 'pending',
    })
    const _event: Event = { id: event.id, createdOn: event.createdOn, updatedOn: event.updatedOn, ...updatedEvent }
    const res = await editItem<FormState, Event>(_event, 'events')
    if (res.success) {
      notificationCtx.showNotification({
        title: 'Success!',
        message: `The event "${updatedEvent.title}" has been successfully updated!`,
        status: 'success',
      })
      onClose()
      res.event?.id ? router.push(`/events/${res.event.id}`) : router.push('/events')
    } else {
      notificationCtx.showNotification({
        title: 'Error!',
        message: res?.message || 'Something went wrong',
        status: 'error',
      })
      return res.errors
    }

    handleClose()
  }

  return (
    <Modal isOpen={isOpen} backdrop="blur" size="xl" onClose={handleClose} isDismissable>
      <ModalContent className="min-w-3/4">
        {() => (
          <>
            <ModalHeader className="flex flex-col gap-1 text-center">Create new Event</ModalHeader>
            <ModalBody className="w-full h-full mb-4">
              <EventForm event={event} onSubmit={submitEvent} />
            </ModalBody>
          </>
        )}
      </ModalContent>
    </Modal>
  )
}

export async function getStaticPaths() {
  const events = await getAll<Event>('events')
  const paths = events?.map(({ id }) => ({ params: { id } }))

  return {
    paths,
    fallback: 'blocking',
  }
}

export async function getStaticProps(context: { params: { id: string } }) {
  const eventId = context.params.id
  const response = await getItem<{ event: Event; message: string }>(eventId, 'events')
  const event = response?.event

  // if (!event) return { notFound: true }

  return {
    props: {
      event: event || null,
    },
    revalidate: 1,
  }
}
