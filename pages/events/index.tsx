import Head from 'next/head'
import { useRouter } from 'next/router'
import { getAll } from '@/helpers/crud'
import { Event } from '@/models/event.model'
import EventList from '@/components/events/event-list'
import EventsSearch from '@/components/events/events-search'
import NavLink from '@/components/nav-link'
import Plus from '@/components/icons/plus'

function EventsPage({ events }: { events: Event[] }) {
  const router = useRouter()

  function findEventsHandler(year: string, month: string) {
    const fullPath = `/events/${year}/${month}`
    router.push(fullPath)
  }

  return (
    <div className="pt-4 w-5/6 m-auto">
      {/* [SEO] */}
      <Head>
        <title>NextJS Events</title>
        <meta name="description" content="Find a lot of great events" />
      </Head>
      <div className="max-w-4xl m-auto relative">
        <NavLink
          path="/events/create"
          className="flex justify-center items-center h-12 w-12 absolute right-0 -top-2 p-0"
          title="Add event">
          <Plus className="size-9 text-green-500" />
        </NavLink>
      </div>
      <h1 className="text-center">All Events</h1>
      <EventsSearch onSearch={findEventsHandler} events={events} />
      <EventList list={events} />
    </div>
  )
}

export async function getStaticProps() {
  const events = await getAll<Event>('events')
  // if (!events) return { notFound: true } // return { redirect: { } }
  return {
    props: {
      events,
    },
    revalidate: 60,
  }
}

// export async function getServerSideProps(context) {
//   const { params, req, res } = context;
//   return {
//     props: {
//       events
//     }
//   }
// }

export default EventsPage
