'use client'

import React, { useContext } from 'react'
import { useRouter } from 'next/navigation'
import { createItem } from '@/helpers/crud'
import EventForm from '@/components/events/event-form'
import type { Event, NewEvent } from '@/models/event.model'
import NotificationContext from '@/store/notification-context'
import { Modal, ModalBody, ModalHeader, ModalContent, useDisclosure } from '@nextui-org/react'

export interface FormState {
  errors?: Partial<Record<keyof Event, string[]>>
  event?: Event
  message?: string
  success: boolean
}

export default function CreateEventPage() {
  const router = useRouter()
  const notificationCtx = useContext(NotificationContext)
  const { isOpen, onClose } = useDisclosure({ isOpen: true })

  function handleClose() {
    onClose()
    router.push('/events')
  }

  async function submitEvent(newEvent: NewEvent) {
    notificationCtx.showNotification({
      title: `Creating the new event "${newEvent.title}"`,
      message: 'In Progress...',
      status: 'pending',
    })
    const res = await createItem<FormState, NewEvent>(newEvent, 'events')
    if (res.success) {
      notificationCtx.showNotification({
        title: 'Success!',
        message: `The new event "${newEvent.title}" has been successfully created!`,
        status: 'success',
      })
      onClose()
      res.event?.id ? router.push(`/events/${res.event.id}`) : router.push('/events')
    } else {
      notificationCtx.showNotification({
        title: 'Error!',
        message: res?.message || 'Something went wrong',
        status: 'error',
      })
      return res.errors
    }
  }

  return (
    <Modal isOpen={isOpen} backdrop="opaque" size="xl" onClose={handleClose} isDismissable>
      <ModalContent className="min-w-3/4">
        {() => (
          <>
            <ModalHeader className="flex flex-col gap-1 text-center">Create new Event</ModalHeader>
            <ModalBody className="w-full h-full mb-4">
              <EventForm onSubmit={submitEvent} />
            </ModalBody>
          </>
        )}
      </ModalContent>
    </Modal>
  )
}
