import Head from 'next/head'
import type { Metadata } from 'next'
import { getAll } from '@/helpers/crud'
import Plus from '@/components/icons/plus'
import NavLink from '@/components/nav-link'
import type { Event } from '@/models/event.model'
import EventList from '@/components/events/event-list'

export const metadata: Metadata = {
  title: 'Events App',
  description: 'List of events',
}

function HomePage({ events }: { events: Event[] }) {
  return (
    <div className="px-8 py-4">
      {/* [SEO] */}
      <Head>
        <title>Events</title>
        <meta name="description" content="Find a lot of great events" />
      </Head>
      <div className="max-w-4xl m-auto flex justify-end">
        <NavLink path="/events/create" className="btn flex justify-between items-center text-2xl h-12 w-48">
          <span>New Event</span>
          <Plus className="size-8" />
        </NavLink>
      </div>
      <div className="mt-4">
        <EventList list={events} />
      </div>
    </div>
  )
}

// export async function getServerSideProps() {
export async function getStaticProps() {
  // const events = getEvents().slice(0, 2)
  const events = await getAll<Event>('events')
  return {
    props: {
      events: events?.slice(0, 2),
    },
    revalidate: 1,
  }
}

export default HomePage
