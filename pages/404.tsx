import Link from 'next/link'

function NotFound() {
  return (
    <div className="text-center">
      <div className="error my-6">
        <p>Page not found!</p>
      </div>
      <Link href="/" className="btn">
        Go back home
      </Link>
    </div>
  )
}

export default NotFound
