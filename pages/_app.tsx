import type { AppProps } from 'next/app'
import Layout from '@/components/layout/layout'
import { SessionProvider } from 'next-auth/react'
import { NextUIProvider } from '@nextui-org/react'
import { NotificationContextProvider } from '@/store/notification-context'

import '@/styles/globals.css'

const locale = 'en-UK'

export default function MyApp({ Component, pageProps: { session, ...pageProps } }: AppProps) {
  return (
    <NextUIProvider locale={locale} className="h-full">
      <NotificationContextProvider>
        <SessionProvider session={session} refetchInterval={5 * 60}>
          <Layout>
            <Component {...pageProps} />
          </Layout>
        </SessionProvider>
      </NotificationContextProvider>
    </NextUIProvider>
  )
}
