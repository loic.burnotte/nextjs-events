import { z } from 'zod'
import { getSession } from 'next-auth/react'
import { NextApiRequest, NextApiResponse } from 'next'
import { hashPassword, verifyPassword } from '@/helpers/auth'
import { extractData, updateData } from '@/helpers/api-utils'
import type { User, NewUserPassword } from '@/models/user.model'

export type ResponseData = {
  message?: string
  user?: User
  users?: User[]
  success: boolean
  error?: z.ZodError<NewUserPassword>
}

const userSchema = z
  .object({
    oldPassword: z.string().min(8, { message: 'Must be 8 or more characters long' }),
    newPassword: z.string().min(8, { message: 'Must be 8 or more characters long' }),
    confirm: z.string(),
  })
  .refine(data => data.newPassword === data.confirm, {
    message: "Passwords don't match",
    path: ['confirm'], // set path of error
  })

async function handler(req: NextApiRequest, res: NextApiResponse<ResponseData>) {
  const users = await extractData<User>('users')

  if (!users) {
    res.status(500).json({ message: 'Failed while getting users', success: false })
    return
  }

  if (req.method !== 'PATCH') return

  const session = await getSession({ req: req })
  if (!session) {
    res.status(401).json({ message: 'Not authenticated', success: false })
    return
  }

  const result = userSchema.safeParse({ ...req.body })
  if (!result.success) {
    res.status(422).json({
      message: 'Invalid password',
      error: result.error,
      success: false,
    })
    return
  }

  const { oldPassword, newPassword } = req.body

  const userEmail = session.user.email
  const user = users.find(u => u.email === userEmail)
  if (!user) {
    res.status(500).json({ message: 'User not found!', success: false })
    return
  }

  const currentPassword = user.password
  const passwordAreEqual = await verifyPassword(oldPassword, currentPassword)
  if (!passwordAreEqual) {
    res.status(403).json({ message: 'Invalid old password', success: false })
    return
  }

  const hashedPassword = await hashPassword(newPassword)
  const updatedUser: User = { ...user, email: userEmail, password: hashedPassword }
  try {
    updateData([...users.filter(u => u.email === userEmail), updatedUser], 'users')
  } catch (e) {
    res.status(500).json({ message: 'User not updated in the database!', success: false })
    return
  }

  res.status(201).json({ message: 'Password updated!', user: updatedUser, success: true })
}

export default handler
