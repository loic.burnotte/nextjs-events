import { z } from 'zod'
import { randomUUID } from 'crypto'
import { Event, NewEvent } from '@/models/event.model'
import { NextApiRequest, NextApiResponse } from 'next'
import { extractData, updateData } from '@/helpers/api-utils'

export type ResponseData = {
  message?: string
  event?: Event
  events?: Event[]
  success: boolean
  error?: z.ZodError<NewEvent>
}

export const eventSchema = z.object({
  address: z.string().min(5, { message: 'Must be 5 or more characters long' }),
  date: z.string().date('Invalid date string'), // ISO date format (YYYY-MM-DD)
  image: z.string().optional(),
  title: z
    .string({
      required_error: 'Title is required',
      invalid_type_error: 'Title must be a string',
    })
    .min(2, { message: 'Must be 2 or more characters long' }),
  description: z.string().optional(),
  summary: z.string().optional(),
})

async function handler(req: NextApiRequest, res: NextApiResponse<ResponseData>) {
  const data = await extractData<Event>('events')

  if (!data) {
    res.status(500).json({ message: 'Failed while getting data', success: false })
    return
  }

  if (req.method === 'POST') {
    const result = eventSchema.safeParse({ ...req.body })
    if (!result.success) {
      res.status(422).json({ message: `Invalid event: ${result.error}`, error: result.error, success: false })
      return
    }

    const today = new Date().toString()
    const newEvent: Event = {
      ...result.data,
      id: randomUUID(),
      createdOn: today,
      updatedOn: today,
    }
    try {
      updateData([...data, newEvent], 'events')
    } catch (e) {
      res.status(500).json({ message: 'Something went wrong when creating the new event', success: false })
      return
    }
    res.status(201).json({ message: 'Success!', event: newEvent, success: true })
  } else {
    res.status(200).json({ events: data, success: true })
  }
}

export default handler
