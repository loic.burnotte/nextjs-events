import type { ResponseData } from '.'
import type { Event } from '@/models/event.model'
import { NextApiRequest, NextApiResponse } from 'next'
import { extractData, updateData } from '@/helpers/api-utils'

async function handler(req: NextApiRequest, res: NextApiResponse<ResponseData>) {
  const eventId = req.query.id
  const events = await extractData<Event>('events')

  if (!events) return res.status(500).json({ message: 'Fail when retrieving events', success: false })
  switch (req.method) {
    case 'GET':
      const selectedEvent = events.find(e => e.id === eventId)
      if (!selectedEvent) res.status(404).json({ message: 'Event not found', success: false })
      res.status(200).json({ event: selectedEvent, success: true })
      break
    case 'DELETE':
      const filteredEvents: Event[] = events.filter(e => e.id !== eventId)
      try {
        updateData(filteredEvents, 'events')
      } catch (e) {
        res.status(500).json({ message: 'Something went wrong when deleting the event', success: false })
        return
      }
      res.status(200).json({ success: true })
      break
    case 'PUT':
      const updatedEvent: Event = {
        ...req.body,
        updatedOn: new Date().toString(),
      }
      const updatedEvents: Event[] = [...events.filter(e => e.id !== req.body.id), updatedEvent]
      try {
        updateData(updatedEvents, 'events')
      } catch (e) {
        res
          .status(500)
          .json({ message: `Something went wrong when updating the event '${updatedEvent.title}'`, success: false })
        return
      }
      res.status(200).json({ event: updatedEvent, success: true })
      break
  }
}

export default handler
