import { z } from 'zod'
import { hashPassword } from '@/helpers/auth'
import type { User } from '@/models/user.model'
import { NextApiRequest, NextApiResponse } from 'next'
import { extractData, updateData } from '@/helpers/api-utils'

type ResponseData = {
  message?: string
  success: boolean
  error?: z.ZodError<User>
}

const userSchema = z
  .object({
    email: z.string().email({ message: 'Must be a valid email address' }),
    password: z.string().min(8, { message: 'Must be 8 or more characters long' }),
    confirm: z.string(),
  })
  .refine(data => data.password === data.confirm, {
    message: "Passwords don't match",
    path: ['confirm'],
  })

async function handler(req: NextApiRequest, res: NextApiResponse<ResponseData>) {
  if (req.method !== 'POST') return

  const result = userSchema.safeParse({ ...req.body })

  if (!result.success) {
    res.status(422).json({
      message: `Invalid input: password should also be at least 8 characters long.`,
      error: result.error,
      success: false,
    })
    return
  }

  const hashedPassword = await hashPassword(req.body.password)
  const users = await extractData<User>('users')
  const existingUser = users?.find(u => u.email === result.data.email)

  if (existingUser) {
    res.status(422).json({ message: 'User already exists!', success: false })
    return
  }

  const _newUser: User = { email: result.data.email, password: hashedPassword, type: req.body.signInMethod }
  const newData = users ? [...users, _newUser] : [_newUser]
  try {
    updateData(newData, 'users')
  } catch (e) {
    res.status(500).json({ message: 'User creation failed when saving in the database!', success: false })
    return
  }

  res.status(201).json({ message: 'Created user!', success: true })
}

export default handler
