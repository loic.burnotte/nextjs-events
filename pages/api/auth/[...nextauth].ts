/* DOCUMENTATION here => https://next-auth.js.org/providers/ */
import NextAuth from 'next-auth'
import { verifyPassword } from '@/helpers/auth'
import type { User } from '@/models/user.model'
import { extractData } from '@/helpers/api-utils'
import CredentialsProvider from 'next-auth/providers/credentials'
// import InstagramProvider from "next-auth/providers/instagram";
// import FacebookProvider from "next-auth/providers/facebook";
// import LinkedInProvider from "next-auth/providers/linkedin";
// import GoogleProvider from "next-auth/providers/google";

export default NextAuth({
  session: {
    strategy: 'jwt',
  },
  callbacks: {
    async jwt({ token, account }) {
      if (account && account.type === 'credentials') {
        token.userId = account.providerAccountId
      }
      return token
    },
    async session({ session, token }) {
      session.user.id = token.userId
      return session
    },
  },
  providers: [
    CredentialsProvider({
      name: 'Credentials',
      async authorize(credentials, _req) {
        const { email, password } = credentials as User

        const usersCollection = await extractData<User>('users')
        const user = usersCollection?.find(u => u.email === email)

        if (!user) {
          throw new Error('No user found!')
        }

        const isValid = await verifyPassword(password, user.password)

        if (!isValid) {
          throw new Error('Could not log you in!')
        }

        // return userService.authenticate(email, password);
        return { email: user.email, type: 'credentials' }
      },
    }),
    // GoogleProvider({
    //   clientId: process.env.GOOGLE_ID,
    //   clientSecret: process.env.GOOGLE_SECRET,
    // })
    // FacebookProvider({
    //   clientId: process.env.FACEBOOK_CLIENT_ID,
    //   clientSecret: process.env.FACEBOOK_CLIENT_SECRET
    // })
    // InstagramProvider({
    //   clientId: process.env.INSTAGRAM_CLIENT_ID,
    //   clientSecret: process.env.INSTAGRAM_CLIENT_SECRET
    // })
    // LinkedInProvider({
    //   clientId: process.env.LINKEDIN_CLIENT_ID,
    //   clientSecret: process.env.LINKEDIN_CLIENT_SECRET
    // })
  ],
})
