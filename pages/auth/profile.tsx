import { useContext } from 'react'
import { NextApiRequest } from 'next'
import NavLink from '@/components/nav-link'
import { useSession, getSession } from 'next-auth/react'
import ProfileForm from '@/components/auth/profile-form'
import type { NewUserPassword } from '@/models/user.model'
import NotificationContext from '@/store/notification-context'
import type { ResponseData } from '../api/user/change-password'
import { useRouter } from 'next/navigation'

async function submitHandler(newUserPassword: NewUserPassword): Promise<ResponseData> {
  return fetch('/api/user/change-password', {
    method: 'PATCH',
    body: JSON.stringify(newUserPassword),
  })
    .then(response => response.json())
    .then(data => {
      return data
    })
}

function ProfilePage() {
  const router = useRouter()
  const notificationCtx = useContext(NotificationContext)
  const { data, status } = useSession()

  async function changePasswordHandler(newUserPassword: NewUserPassword) {
    notificationCtx.showNotification({
      title: 'Updating password',
      message: 'In Progress...',
      status: 'pending',
    })
    const res: ResponseData = await submitHandler(newUserPassword)

    if (res.success) {
      notificationCtx.showNotification({
        title: 'Success!',
        message: 'The password has been updated!',
        status: 'success',
      })
      router.push('/')
    } else {
      notificationCtx.showNotification({
        title: 'Error!',
        message: res?.message || 'Something went wrong',
        status: 'error',
      })
    }
  }

  function render() {
    switch (status) {
      case 'loading':
        return <h2>Loading...</h2>
      case 'unauthenticated':
        return (
          <>
            <h1>Not signed in</h1>
            <NavLink path="/auth" className="btn mt-6">
              Sign In
            </NavLink>
          </>
        )
      case 'authenticated':
      default:
        return (
          <>
            <h1>Your User Profile</h1>
            <ProfileForm user={data.user} onChangePassword={changePasswordHandler} />
          </>
        )
    }
  }

  return <section className="text-center mt-6">{render()}</section>
}

export async function getServerSideProps(context: { req: NextApiRequest }) {
  const session = await getSession({ req: context.req })

  if (!session) {
    return {
      redirect: {
        destination: '/auth',
        permanent: false,
      },
    }
  }

  return {
    props: { session },
  }
}

export default ProfilePage
