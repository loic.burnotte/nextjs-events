import { formatDate, formatToSave } from './common'

describe('formatDate', () => {
  const cases: [date: string, result: string][] = [
    ['2024-02-25', 'February 25, 2024'],
    ['2024-12-15', 'December 15, 2024'],
    ['2024-12', 'December 1, 2024'],
    ['2024', 'January 1, 2024'],
  ]

  test.each(cases)('%#: %p should be formatted to %p', (date, expected) => {
    const response = formatDate(date)
    expect(response).toStrictEqual(expected)
  })
})

describe('formatToSave', () => {
  const cases: [date: string | Date, result: string][] = [
    ['02-25-2024', '2024-02-25'],
    [new Date('2024-02-25'), '2024-02-25'],
    ['2024-10-15T12:45:02.018Z', '2024-10-15'],
    ['2024-12', '2024-12-01'],
    ['2014', '2014-01-01'],
  ]

  test.each(cases)('%#: %p should be formatted to %p', (date, expected) => {
    const response = formatToSave(date)
    expect(response).toStrictEqual(expected)
  })
})
