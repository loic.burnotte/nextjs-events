import path from 'path'
import type { TableName } from './api-utils'

export const formatDate = (date: string) =>
  new Date(date).toLocaleDateString('en-US', {
    day: 'numeric',
    month: 'long',
    year: 'numeric',
  })

export const formatToSave = (date: string | Date) => {
  let _date = date
  if (!(_date instanceof Date)) _date = new Date(date)
  const year = _date.getFullYear()
  const month = _date.getMonth() + 1
  const day = _date.getDate()

  return `${year}-${month < 10 ? '0' + month : month}-${day < 10 ? '0' + day : day}`
}

export function buildPath(tableName: TableName) {
  return path.join(process.cwd(), 'data', `${tableName}.json`)
}
