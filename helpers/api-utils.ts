import fs from 'fs'
import { buildPath } from './common'

export type TableName = 'events' | 'users'

/* -[CRUD]- */

// const filePath = path.join(process.cwd(), 'data', 'events.json')
// const fileData = fs.readFileSync(filePath)
// const data = JSON.parse(fileData.toString())
// data.push(newEvent)
// fs.writeFileSync(filePath, JSON.stringify(data))

// READ
export async function extractData<T>(tableName: TableName): Promise<T[] | undefined> {
  const _filePath = buildPath(tableName)
  try {
    const fileData = fs.readFileSync(_filePath, 'utf8')
    const data = JSON.parse(fileData.toString())
    return data || []
  } catch (error) {
    // eslint-disable-next-line no-console
    console.error(`Error reading ${tableName} file: ${error}`)
    return []
  }
}

// CREATE + UPDATE + DELETE +
export async function updateData<T>(updateData: T[], tableName: TableName) {
  try {
    const _filePath = buildPath(tableName)
    fs.writeFileSync(_filePath, '') // clean file first
    fs.writeFileSync(_filePath, JSON.stringify(updateData))
  } catch (error) {
    // eslint-disable-next-line no-console
    console.error(`Error writing ${tableName} file: ${error}`)
  }
}
