import type { TableName } from './api-utils'

export interface Filters {
  year: number
  month: number
}

/*    ==================================================================    */
/*    ============================== READ ==============================    */
/*    ==================================================================    */
export async function getAll<T>(tableName: TableName): Promise<T[]> {
  return fetch(`${process.env.URL}/api/${tableName}`, { next: { tags: [tableName] } })
    .then(response => response.json())
    .then(data => {
      return data[tableName]
    })
}

export async function getItem<T>(id: string, tableName: TableName): Promise<T | undefined> {
  return fetch(`${process.env.URL}/api/${tableName}/${id}`, { next: { tags: [tableName] } })
    .then(response => response.json())
    .then((data: T | undefined) => {
      return data
    })
}

export async function getFilteredData<T extends { date: string }>({
  filters: { year, month },
  tableName,
}: {
  filters: Filters
  tableName: TableName
}): Promise<T[]> {
  const data = await getAll<T>(tableName)
  const filteredByYear = data.filter(d => new Date(d.date).getFullYear() === year) || []
  return filteredByYear.filter(d => new Date(d.date).getMonth() === month - 1) || []
}

/*    ==================================================================    */
/*    ============================== WRITE ==============================    */
/*    ==================================================================    */
export async function createItem<T, U = unknown>(body: U, tableName: TableName): Promise<T> {
  return fetch(`/api/${tableName}`, {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then(response => response.json())
    .then(data => {
      return data
    })
}

export async function editItem<T, U extends { id: string }>(body: U, tableName: TableName): Promise<T> {
  return fetch(`/api/${tableName}/${body.id}`, {
    method: 'PUT',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then(response => response.json())
    .then(data => {
      return data
    })
}

export async function deleteItem(id: string, tableName: TableName): Promise<{ message?: string; success: boolean }> {
  return fetch(`/api/${tableName}/${id}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
    },
    next: { tags: [tableName] },
  })
    .then(response => response.json())
    .then(data => {
      return data
    })
}
