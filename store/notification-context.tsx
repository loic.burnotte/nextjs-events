import { PropsWithChildren, createContext, useEffect, useState } from 'react'

export interface Notification {
  title: string
  message: string
  status: 'success' | 'pending' | 'error'
}
export interface NotificationContextState {
  notification: Notification | null
  showNotification: (NotificationDate: Notification) => void
  hideNotification: () => void
}

const NotificationContext = createContext<NotificationContextState>({
  notification: null,
  showNotification: (_notificationData: Notification) => {},
  hideNotification: () => {},
})

export function NotificationContextProvider({ children }: PropsWithChildren) {
  const [activeNotification, setActiveNotification] = useState<Notification | null>(null)

  useEffect(() => {
    if (activeNotification && (activeNotification.status === 'success' || activeNotification.status === 'error')) {
      const timer = setTimeout(() => {
        setActiveNotification(null)
      }, 3000)

      return () => {
        clearTimeout(timer)
      }
    }
  }, [activeNotification])

  function showNotificationHandler(notificationData: Notification) {
    setActiveNotification(notificationData)
  }

  function hideNotificationHandler() {
    setActiveNotification(null)
  }

  const context = {
    notification: activeNotification,
    showNotification: showNotificationHandler,
    hideNotification: hideNotificationHandler,
  }

  return <NotificationContext.Provider value={context}>{children}</NotificationContext.Provider>
}

export default NotificationContext
