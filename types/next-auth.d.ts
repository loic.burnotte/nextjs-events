import { JWT } from 'next-auth/jwt'
import type { User } from '@/models/user.model'
import NextAuth, { DefaultSession, DefaultJWT } from 'next-auth'

declare module 'next-auth' {
  interface Session extends DefaultSession {
    user: {
      id: string
    } & User
  }
}

declare module 'next-auth/jwt' {
  interface JWT {
    userId: string
  }
}
