'use client'

import Link from 'next/link'
import { useRouter } from 'next/router'

interface NavLinkProps {
  path: string
  children: React.ReactNode
  title?: string
  className?: string
}

function NavLink({ path, children, title, className }: NavLinkProps) {
  const router = useRouter()

  return (
    <Link href={path} className={`link ${router.pathname === path ? 'active' : undefined} ${className}`} title={title}>
      {children}
    </Link>
  )
}
export default NavLink
