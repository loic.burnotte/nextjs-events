import { signIn } from 'next-auth/react'
import { Input } from '@nextui-org/react'
import { useRouter } from 'next/navigation'
import type { UserType } from '@/models/user.model'
import { useState, useRef, FormEvent, useContext } from 'react'
import NotificationContext from '@/store/notification-context'

async function createUser(email?: string, password?: string, confirm?: string) {
  const response = await fetch('/api/auth/signup', {
    method: 'POST',
    body: JSON.stringify({ email, password, confirm }),
    headers: {
      'Content-Type': 'application/json',
    },
  })

  const data = await response.json()

  if (!response.ok) {
    throw new Error(data.message || 'Something went wrong!')
  }

  return data
}

function AuthForm() {
  const router = useRouter()
  const notificationCtx = useContext(NotificationContext)

  const [signInMethod, setSignInMethod] = useState<UserType>('credentials')
  const [isLoading, setIsLoading] = useState(false)
  const [error, setError] = useState<string>()

  const emailInputRef = useRef<HTMLInputElement>(null)
  const passwordInputRef = useRef<HTMLInputElement>(null)
  const confirmPasswordInputRef = useRef<HTMLInputElement>(null)

  const [isLogin, setIsLogin] = useState(true)

  function switchAuthModeHandler() {
    setIsLogin(prevState => !prevState)
  }

  async function submitHandler(event: FormEvent<HTMLFormElement>) {
    event.preventDefault()
    setError(undefined)
    setIsLoading(true)

    const enteredEmail = emailInputRef.current?.value
    const enteredPassword = passwordInputRef.current?.value

    notificationCtx.showNotification({
      title: 'Updating password',
      message: 'In Progress...',
      status: 'pending',
    })

    let res = undefined

    if (isLogin) {
      res = await signIn(signInMethod, { redirect: false, email: enteredEmail, password: enteredPassword })
      if (res?.error) {
        router.replace('/auth/profile')
        setError(res.error)
      }
    } else {
      const confirmPassword = confirmPasswordInputRef.current?.value
      try {
        res = await createUser(enteredEmail, enteredPassword, confirmPassword)
      } catch (e) {
        setError('Connection failed')
      }
    }
    if (res?.ok) {
      notificationCtx.showNotification({
        title: 'Success!',
        message: 'The password has been updated!',
        status: 'success',
      })
      router.push('/')
    } else {
      notificationCtx.showNotification({
        title: 'Error!',
        message: res?.error || 'Something went wrong',
        status: 'error',
      })
      setError(res?.error)
    }
    setIsLoading(false)
  }

  return (
    <>
      <section className="w-3/4 max-w-2xl mx-auto mt-20 text-center bg-white p-6 rounded-lg shadow-sm">
        <h1 className="mb-4">{isLogin ? 'Login' : 'Sign Up'}</h1>
        <form onSubmit={submitHandler} className="flex flex-col gap-6">
          <Input
            name="email"
            label="Email"
            type="email"
            labelPlacement="outside"
            placeholder="Your email"
            ref={emailInputRef}
            // isInvalid={!!formState?.errors?.email}
            // errorMessage={formState?.errors?.email?.join(', ')}
            isRequired
          />
          <Input
            name="password"
            label="Password"
            type="password"
            labelPlacement="outside"
            placeholder="Your password"
            ref={passwordInputRef}
            // isInvalid={!!formState?.errors?.password}
            // errorMessage={formState?.errors?.password?.join(', ')}
            isRequired
          />
          {!isLogin && (
            <Input
              name="confirm"
              label="Password"
              type="password"
              labelPlacement="outside"
              placeholder="Confirm password"
              ref={confirmPasswordInputRef}
              // isInvalid={!!formState?.errors?.confirm}
              // errorMessage={formState?.errors?.confirm?.join(', ')}
              isRequired
            />
          )}
          {error && <div className="text-red-500">{error}</div>}
          {isLoading ? (
            <button className="btn">Loading...</button>
          ) : (
            <button className="btn" onClick={() => setSignInMethod('credentials')}>
              {isLogin ? 'Login' : 'Create Account'}
            </button>
          )}

          <button type="button" className="text-slate-700 font-semibold text-md" onClick={switchAuthModeHandler}>
            {isLogin ? 'Create new account' : 'Login with existing account'}
          </button>
        </form>
      </section>
      <div className="w-3/4 max-w-2xl mx-auto bg-white rounded-lg p-6 mt-6 shadow-sm">
        <h2 className="text-center font-semibold">Signin with</h2>
        <div className="flex flex-row mt-4 justify-between">
          <button
            className="rounded-lg border-2 px-4 py-2 border-slate-500"
            onClick={() => setSignInMethod('facebook')}>
            Facebook
          </button>
          <button className="rounded-lg border-2 px-4 py-2 border-slate-500" onClick={() => setSignInMethod('google')}>
            Google
          </button>
          <button
            className="rounded-lg border-2 px-4 py-2 border-slate-500"
            onClick={() => setSignInMethod('instagram')}>
            Instagram
          </button>
          <button
            className="rounded-lg border-2 px-4 py-2 border-slate-500"
            onClick={() => setSignInMethod('linkedin')}>
            Linkedin
          </button>
        </div>
      </div>
    </>
  )
}

export default AuthForm
