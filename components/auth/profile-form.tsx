import { useRef, useState } from 'react'
import { Button, Input } from '@nextui-org/react'
import type { User, NewUserPassword } from '@/models/user.model'

interface ProfileFormProps {
  user: User
  onChangePassword: (newUserPassword: NewUserPassword) => void
}

function ProfileForm({ user, onChangePassword }: ProfileFormProps) {
  const oldPasswordInputRef = useRef<HTMLInputElement>(null)
  const newPasswordInputRef = useRef<HTMLInputElement>(null)
  const confirmNewPasswordInputRef = useRef<HTMLInputElement>(null)

  const [isLoading, setIsLoading] = useState(false)

  async function resetPassword() {
    setIsLoading(true)

    const enteredOldPassword = oldPasswordInputRef.current?.value
    const enteredNewPassword = newPasswordInputRef.current?.value
    const enteredConfirmPassword = confirmNewPasswordInputRef.current?.value

    if (!enteredOldPassword || !enteredNewPassword || !enteredConfirmPassword) {
      return
    }

    await onChangePassword({
      oldPassword: enteredOldPassword,
      newPassword: enteredNewPassword,
      confirm: enteredConfirmPassword,
    })

    setIsLoading(false)
  }

  return (
    <>
      <h4 className="my-6">
        User email: <span className="text-slate-500">{user.email}</span>
      </h4>
      {/* {user?.type === 'credentials' && ( */}
      <h4 className="font-semibold mb-6">Reset password</h4>
      <div className="w-3/4 max-w-2xl mx-auto text-center bg-white p-6 rounded-lg shadow-sm">
        <form onSubmit={resetPassword} className="flex flex-col gap-6">
          <Input
            name="oldPassword"
            label="Old Password"
            type="password"
            labelPlacement="outside"
            placeholder="Your old password"
            ref={oldPasswordInputRef}
            // isInvalid={!!formState?.errors?.password}
            // errorMessage={formState?.errors?.password?.join(', ')}
            isRequired
          />
          <Input
            name="newPassword"
            label="New Password"
            type="password"
            labelPlacement="outside"
            placeholder="Your new password"
            ref={newPasswordInputRef}
            // isInvalid={!!formState?.errors?.password}
            // errorMessage={formState?.errors?.password?.join(', ')}
            isRequired
          />
          <Input
            name="confirm"
            label="Confirm New Password"
            type="password"
            labelPlacement="outside"
            placeholder="Confirm new password"
            ref={confirmNewPasswordInputRef}
            // isInvalid={!!formState?.errors?.confirm}
            // errorMessage={formState?.errors?.confirm?.join(', ')}
            isRequired
          />
          <Button isLoading={isLoading} className="font-semibold text-slate-100 text-lg" color="success">
            Save
          </Button>
        </form>
      </div>
      {/* )} */}
    </>
  )
}

export default ProfileForm
