import { useState } from 'react'
import Trash from './icons/trash'
import ConfirmModal from './confirm-modal'

interface ConfirmModalProps {
  message?: string
  className?: string
  onConfirm: () => void
}

function DeleteIcon({ message, className, onConfirm }: ConfirmModalProps) {
  const [openConfirmModal, setOpenConfirmModal] = useState(false)

  return (
    <>
      <div className={`${className} cursor-pointer`} onClick={() => setOpenConfirmModal(true)} title="Delete">
        <Trash />
      </div>
      <ConfirmModal
        isOpen={openConfirmModal}
        close={() => setOpenConfirmModal(false)}
        header={message}
        onConfirm={onConfirm}
      />
    </>
  )
}

export default DeleteIcon
