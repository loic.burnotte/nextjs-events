import Image from 'next/image'
import NavLink from '../nav-link'
import Calendar from '../icons/calendar'
import Location from '../icons/location'
import { formatDate } from '@/helpers/common'
import ArrowRight from '../icons/arrow-right'
import type { Event } from '@/models/event.model'
import UnknownImage from '../icons/unknown-image'

function EventItem({ item }: { item: Event }) {
  const { title, image, address, date, id } = item
  const exploreLink = `/events/${id}`

  return (
    <li className="flex flex-col md:flex-row gap-4 overflow-hidden shadow rounded-md bg-white mb-12">
      <div className="w-full md:w-2/5 relative h-52">
        {image ? (
          <Image src={`/${image}`} alt={title} fill quality={50} className="object-cover" sizes="max-width: 768px" />
        ) : (
          <div className="w-full flex justify-center h-full items-center">
            <UnknownImage size="size-40" />
          </div>
        )}
      </div>

      <div className="w-full md:w-3/5 p-4 flex flex-col gap-1 relative">
        <h2 className="mb-2">{title}</h2>
        <div className="flex flex-row gap-2">
          <Calendar />
          <time className="font-medium">{formatDate(date)}</time>
        </div>
        <div className="italic flex flex-row gap-2 items-center whitespace-pre-line">
          <Location />
          <address>{address.replace(', ', '\n')}</address>
        </div>
        <div className="absolute right-4 bottom-4">
          <NavLink path={exploreLink} className="w-full md:w-44 flex flex-row justify-center gap-2 btn">
            <span>Explore Event</span>
            <ArrowRight />
          </NavLink>
        </div>
      </div>
    </li>
  )
}

export default EventItem
