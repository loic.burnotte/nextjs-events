'use client'

import React, { useState } from 'react'
import { formatToSave } from '@/helpers/common'
import { parseDate } from '@internationalized/date'
import type { FormState } from '@/pages/events/create'
import type { Event, NewEvent } from '@/models/event.model'
import { Input, Textarea, DatePicker, Button } from '@nextui-org/react'

interface EventFormProps {
  event?: Event | null
  onSubmit: (newEvent: NewEvent) => Promise<FormState['errors'] | undefined>
}

export default function EventForm({ event, onSubmit }: EventFormProps) {
  const [isLoading, setIsLoading] = useState(false)
  const [errors, setErrors] = useState<FormState['errors']>()
  const defaultEvent: NewEvent = event
    ? event
    : { address: '', date: '', image: '', title: '', description: '', summary: '' }
  const [newEvent, setNewEvent] = useState<NewEvent>(defaultEvent)

  async function submitEvent(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault()
    setIsLoading(true)
    const _errors = await onSubmit(newEvent)
    if (_errors) setErrors(_errors)
    setIsLoading(false)
  }

  return (
    <form onSubmit={submitEvent}>
      <div className="flex flex-col gap-4 w-full">
        <Input
          name="title"
          label="Title"
          labelPlacement="outside"
          placeholder="Title"
          value={newEvent?.title}
          onChange={e => setNewEvent(prevState => ({ ...prevState, title: e.target.value }))}
          isInvalid={!!errors?.title}
          errorMessage={errors?.title?.join(', ')}
          isRequired
        />

        <Input
          name="address"
          label="Address"
          labelPlacement="outside"
          placeholder="Address"
          value={newEvent?.address}
          onChange={e => setNewEvent(prevState => ({ ...prevState, address: e.target.value }))}
          isInvalid={!!errors?.address}
          errorMessage={errors?.address?.join(', ')}
          isRequired
        />

        <DatePicker
          name="date"
          label="Date"
          labelPlacement="outside"
          value={newEvent.date ? parseDate(newEvent.date) : undefined}
          onChange={e =>
            setNewEvent(prevState => ({
              ...prevState,
              date: formatToSave(new Date(e.year, e.month, e.day)), //`${e.year}-${e.month < 10 ? '0' + e.month : e.month}-${e.day < 10 ? '0' + e.day : e.day}`,
            }))
          }
          isInvalid={!!errors?.date}
          errorMessage={errors?.date?.join(', ')}
          isRequired
        />

        <Input
          name="summary"
          label="Summary"
          labelPlacement="outside"
          placeholder="Summary"
          value={newEvent?.summary}
          onChange={e => setNewEvent(prevState => ({ ...prevState, summary: e.target.value }))}
          isInvalid={!!errors?.summary}
          errorMessage={errors?.summary?.join(', ')}
        />

        <Textarea
          name="description"
          label="Description"
          labelPlacement="outside"
          placeholder="Describe your event"
          value={newEvent?.description}
          onChange={e => setNewEvent(prevState => ({ ...prevState, description: e.target.value }))}
          isInvalid={!!errors?.description}
          errorMessage={errors?.description?.join(', ')}
        />

        <Button isLoading={isLoading} className="font-semibold text-slate-100 text-lg" color="success">
          {event ? 'Save' : 'Create'}
        </Button>
      </div>
    </form>
  )
}
