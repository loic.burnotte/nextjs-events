import Link from 'next/link'
import { formatDate } from '@/helpers/common'

interface ResultsTitleProps {
  date: Date
  className?: string
}

function ResultsTitle({ date, className }: ResultsTitleProps) {
  return (
    <section className={`text-center ${className}`}>
      <h1 className="mb-4">Events in {formatDate(date.toString())}</h1>
      <Link href="/events" className="btn">
        Show all events
      </Link>
    </section>
  )
}

export default ResultsTitle
