import { FormEvent, useRef, useState } from 'react'
import type { Event } from '@/models/event.model'

const allMonths = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
]

interface EventsSearchProps {
  events: Event[]
  onSearch: (year: string, month: string) => void
}

function EventsSearch({ events, onSearch }: EventsSearchProps) {
  const [selectedYear, setSelectedYear] = useState(new Date().getFullYear())
  const yearInputRef = useRef<HTMLSelectElement>(null)
  const monthInputRef = useRef<HTMLSelectElement>(null)

  const years = events.reduce<number[]>((acc, curr) => {
    const year = new Date(curr.date).getFullYear()
    if (!acc.find(a => a === year)) acc.push(year)
    return acc
  }, [])

  function handleSelect(e: FormEvent<HTMLSelectElement>) {
    'value' in e.target && setSelectedYear(Number(e.target.value))
  }

  const months = events.reduce<number[]>((acc, curr) => {
    const year = new Date(curr.date).getFullYear()
    const month = new Date(curr.date).getMonth()
    if ((year in years || year === selectedYear) && !acc.includes(month)) acc.push(month)
    return acc
  }, [])

  function submitHandler(event: FormEvent<HTMLFormElement>) {
    event.preventDefault()

    const selectYear = yearInputRef.current?.value || new Date().getFullYear().toString()
    const selectedMonth = monthInputRef.current?.value || new Date().getMonth().toString()
    onSearch(selectYear, selectedMonth)
  }

  return (
    <form className="max-w-4xl m-auto mt-4 mb-8 p-4 shadow bg-white rounded-md center gap-4" onSubmit={submitHandler}>
      <div className="flex flex-col md:flex-row gap-4">
        <div className="center-el gap-4 w-full md:w-2/5">
          <label htmlFor="year" className="font-semibold">
            Year
          </label>
          <select
            id="year"
            className="bg-white rounded-md p-1 w-full "
            ref={yearInputRef}
            onChange={handleSelect}
            defaultValue={selectedYear}>
            {years.map(year => (
              <option key={year.toString()} value={year}>
                {year}
              </option>
            ))}
          </select>
        </div>

        <div className="center-el gap-4 w-full md:w-2/5">
          <label htmlFor="month" className="font-semibold">
            Month
          </label>
          <select
            id="month"
            className="bg-white rounded-md p-1 w-full "
            ref={monthInputRef}
            defaultValue={new Date().getMonth() + 1}>
            {months.map(month => (
              <option key={allMonths[month]} value={month + 1}>
                {allMonths[month]}
              </option>
            ))}
          </select>
        </div>

        <button type="submit" className="w-full md:w-1/5 btn">
          Find Events
        </button>
      </div>
    </form>
  )
}

export default EventsSearch
