import EventItem from './event-item'
import type { Event } from '@/models/event.model'

function EventList({ list }: { list?: Event[] }) {
  if (!list?.length) return null

  return (
    <ul className="max-w-4xl overflow-y-auto m-auto">
      {list.map(item => (
        <EventItem key={item.id} item={item} />
      ))}
    </ul>
  )
}

export default EventList
