import ReactDOM from 'react-dom'
import { useContext } from 'react'
import NotificationContext from '@/store/notification-context'
import type { Notification } from '@/store/notification-context'

interface NotificationProps {
  notification: Notification
}

const colors = { success: 'bg-green-500', pending: 'bg-blue-500', error: 'bg-red-500' }

function Notification({ notification }: NotificationProps) {
  const notificationCtx = useContext(NotificationContext)
  const { message, status, title } = notification

  return ReactDOM.createPortal(
    <div
      className={`absolute bottom-0 left-0 right-0 w-full h-20 text-white flex flex-row justify-between items-center p-8 ${colors[status]}`}
      onClick={notificationCtx.hideNotification}>
      <h3 className="font-semibold">{title}</h3>
      <span className="text-lg">{message}</span>
    </div>,
    document.getElementById('notifications')!
  )
}

export default Notification
