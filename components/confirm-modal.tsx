import { Modal, ModalBody, ModalContent, ModalFooter, ModalHeader } from '@nextui-org/react'

interface ConfirmModalProps {
  message?: string
  header?: string
  isOpen?: boolean
  close?: () => void
  onConfirm: () => void
}

function ConfirmModal({ message, header, isOpen, close, onConfirm }: ConfirmModalProps) {
  return (
    <Modal isOpen={isOpen} backdrop="opaque" size="xl" onClose={close} isDismissable>
      <ModalContent className="min-w-3/4">
        {() => (
          <>
            {header && <ModalHeader className="flex flex-col gap-1 text-center">{header}</ModalHeader>}
            {message && <ModalBody className="w-full h-full mb-4">{message}</ModalBody>}
            <ModalFooter>
              <button className="btn bg-red-500" onClick={close}>
                No
              </button>
              <button className="btn" onClick={onConfirm}>
                Yes
              </button>
            </ModalFooter>
          </>
        )}
      </ModalContent>
    </Modal>
  )
}

export default ConfirmModal
