import Image from 'next/image'
import User from '../icons/user'
import NavLink from '../nav-link'
import { useRouter } from 'next/navigation'
import logo from '@/public/images/human.jpg'
import { signOut, useSession } from 'next-auth/react'
import { Button, Dropdown, DropdownItem, DropdownMenu, DropdownTrigger } from '@nextui-org/react'

function Navbar() {
  const { status } = useSession()
  const router = useRouter()

  return (
    <div className="py-6 px-20 h-20 w-full flex flex-row items-center justify-between gap-12 bg-slate-800">
      <NavLink path="/">
        <Image
          src={logo}
          alt="logo"
          width={60}
          height={60}
          className="rounded-full border-solid border-4 border-opacity-70 border-slate-200"
        />
      </NavLink>
      <NavLink path="/events">
        <h1>Browse All Events</h1>
      </NavLink>
      {status === 'unauthenticated' && (
        <NavLink path="/auth">
          <h3 className="text-white">Login</h3>
        </NavLink>
      )}
      {status === 'loading' && <Button isLoading className="text-white bg-transparent" />}
      {status === 'authenticated' && (
        <Dropdown placement="bottom-end">
          <DropdownTrigger>
            <Button isIconOnly className="bg-transparent text-white">
              <User />
            </Button>
          </DropdownTrigger>
          <DropdownMenu disallowEmptySelection selectionMode="single" className="max-w-[200px]">
            <DropdownItem key="1" onClick={() => signOut()}>
              Logout
            </DropdownItem>
            <DropdownItem key="2" onClick={() => router.push('/auth/profile')}>
              Profile
            </DropdownItem>
          </DropdownMenu>
        </Dropdown>
      )}
    </div>
  )
}

export default Navbar
