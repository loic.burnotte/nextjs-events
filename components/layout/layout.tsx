import { useContext } from 'react'
import Navbar from './navbar'
import Notification from '../notification'
import NotificationContext from '@/store/notification-context'

export default function Layout({ children }: { children: React.ReactNode }) {
  const notificationCtx = useContext(NotificationContext)

  return (
    <>
      <Navbar />
      <main className="bg-gradient-to-b from-orange-50/0 to-orange-500/50 h-full overflow-auto">{children}</main>
      {notificationCtx.notification && <Notification notification={notificationCtx.notification} />}
    </>
  )
}
