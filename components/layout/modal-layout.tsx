import { useRouter } from 'next/router'

export default function ModalLayout({ children }: { children: React.ReactNode }) {
  const router = useRouter()
  const { create } = router.query
  return (
    <>
      {create ? null : null}
      {children}
    </>
  )
}
