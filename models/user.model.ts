export type UserType = 'credentials' | 'google' | 'facebook' | 'instagram' | 'linkedin'

export type User = { id?: string; email: string; password: string; type?: UserType }

export type NewUserPassword = {
  oldPassword: string
  newPassword: string
  confirm: string
}
