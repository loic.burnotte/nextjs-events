export interface NewEvent {
  address: string
  date: string
  description?: string
  image?: string
  summary?: string
  title: string
}

export interface Event extends NewEvent {
  createdOn: string
  id: string
  updatedOn: string
}
